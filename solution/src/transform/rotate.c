#include "rotate.h"
#include <malloc.h>

struct image rotate_image(const struct image image) {
    struct image transformed_image;
    transformed_image.height = image.width;
    transformed_image.width = image.height;
    transformed_image.data = malloc(transformed_image.width * transformed_image.height * sizeof(struct pixel));

    for (size_t i = 0; i < image.height; i++) {

        for (size_t j = 0; j < image.width; j++) {
            transformed_image.data[(image.width - j - 1) * image.height + i] = image.data[i * image.width + j];
        }

    }
    return transformed_image;
}

#ifndef _ROTATE_H_
#define _ROTATE_H_

#include "../image-formats/image.h"

struct image rotate_image(const struct image image);

#endif

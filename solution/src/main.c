#include <stdio.h>
#include <stdlib.h>
#include "image-formats/bmp.h"
#include "image-formats/image.h"
#include "utils/error.h"
#include "utils/files.h"
#include "transform/rotate.h"

static void destroy_and_exit(struct image image1, struct image image2) {
    destroy_image(&image1);
    destroy_image(&image2);
    exit(-1);
}
int main(int argc, char** argv) {
    if (argc < 3) {
        print_err("Недостаточно аргументов");
    } else if (argc > 3) {
        print_err("Слишком много аргументов");
    } else {
        struct image image = {0}, transformed_image = {0};

        switch (file_load(argv[1], &image, from_bmp)) {
            case LOAD_WRONG_FORMAT:
                print_err("Неправильный формат входного файла");
                destroy_and_exit(image, transformed_image);
            case LOAD_NO_SUCH_FILE_OR_DIRECTORY:
                print_err("Неправильный путь входного файла");
                destroy_and_exit(image, transformed_image);
            case LOAD_OK:
                break;
        }
        transformed_image = rotate_image(image);
        switch (file_save(argv[2], &transformed_image, to_bmp)) {
            case SAVE_WRONG_FORMAT:
                print_err("Неправильный формат выходного файла");
                destroy_and_exit(image, transformed_image);
            case SAVE_NO_SUCH_FILE_OR_DIRECTORY:
                print_err("Неправильный путь выходного файла");
                 destroy_and_exit(image, transformed_image);
            case SAVE_OK:
                break;
        }
    }
    return 0;
}




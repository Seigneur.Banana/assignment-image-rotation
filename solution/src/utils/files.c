#include "files.h"

enum load_status file_load(const char* filename, struct image* image, enum read_status transform(FILE*, struct image*)) {
    FILE* file;
    file = fopen(filename, "rb");
    if (file) {
        switch (transform(file, image)) {
            case READ_INVALID_HEADER:
                fclose(file);
                print_err("Ошибка бмп заголовка");
                return LOAD_WRONG_FORMAT;
            case READ_INVALID_BITS:
                fclose(file);
                print_err("Ошибка битов");
                return LOAD_WRONG_FORMAT;
            case READ_CORRUPTED_BMP:
                fclose(file);
                print_err("Кривой файл!");
                return LOAD_WRONG_FORMAT;
            case READ_OK:
                fclose(file);
                return LOAD_OK;
        }
    }
    return LOAD_NO_SUCH_FILE_OR_DIRECTORY;
}

enum save_status file_save(const char* filename, struct image const* image, enum write_status transform(FILE*, struct image const*)) {
    FILE* file;
    file = fopen(filename, "wb");
    if (file) {
        switch (transform(file, image)) {
            case WRITE_CORRUPTED_IMAGE:
                print_err("Ошибка заголовка или картинки");
                return SAVE_WRONG_FORMAT;
            case WRITE_OK:
                fclose(file);
                return SAVE_OK;
        }
    }
    return SAVE_NO_SUCH_FILE_OR_DIRECTORY;
}

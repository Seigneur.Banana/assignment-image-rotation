#ifndef _FILES_H_
#define _FILES_H_

#include "../image-formats/image.h"
#include "../image-formats/bmp.h"
#include "error.h"

enum load_status {
    LOAD_OK = 0,
    LOAD_WRONG_FORMAT,
    LOAD_NO_SUCH_FILE_OR_DIRECTORY
};

enum save_status {
    SAVE_OK = 0,
    SAVE_WRONG_FORMAT,
    SAVE_NO_SUCH_FILE_OR_DIRECTORY
};

enum load_status file_load(const char* filename, struct image* image, enum read_status transform(FILE*, struct image*));
enum save_status file_save(const char* filename, struct image const* image, enum write_status transform(FILE*, struct image const*));

#endif

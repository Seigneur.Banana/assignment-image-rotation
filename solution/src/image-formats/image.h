#ifndef _IMAGE_H_
#define _IMAGE_H_

#include <inttypes.h>

struct pixel {
  uint8_t b, g, r;
};

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct image create_image(uint64_t width, uint64_t height, struct pixel* const data);
void destroy_image(struct image* image);

#endif

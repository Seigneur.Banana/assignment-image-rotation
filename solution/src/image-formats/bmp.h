#ifndef _BMP_H_
#define _BMP_H_

#include "../image-formats/image.h"
#include <stdio.h>

enum read_status {
    READ_OK = 0,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_CORRUPTED_BMP
};

enum write_status {
    WRITE_OK = 0,
    WRITE_CORRUPTED_IMAGE
};

enum read_status from_bmp(FILE* in, struct image* image);
enum write_status to_bmp(FILE* out, struct image const* image);

#endif

#include "image.h"
#include <malloc.h>


struct image create_image(uint64_t width, uint64_t height, struct pixel* const data) {
    return (struct image) {.width = width, .height = height, .data = data};
}

void destroy_image(struct image* image) {
    if (image->data != NULL) {
        free(image->data);
    }
}
